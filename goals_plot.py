#!/usr/bin/python3
import numpy as np
import pandas as pd
import matplotlib.patches as patches
import matplotlib.path as path

import matplotlib.pyplot as plt 
plt.rc("font", size=14)
import seaborn as sns
sns.set(style="white")
sns.set(style="whitegrid", color_codes=True)


df = pd.read_csv("data/odds_series_timesteps_final_ru_rnans_br.csv", sep=',', quotechar='"', encoding="latin-1")
df['score_home'] = pd.to_numeric(df['score_home'], downcast='float', errors='ignore')
df['score_away'] = pd.to_numeric(df['score_away'], downcast='float', errors='ignore')
df['total_score'] = df['score_home'] + df['score_away']
print (df['total_score'])

sns.set(font_scale=1.4)

f, ax = plt.subplots(figsize=(18, 16))

a = sns.countplot(x='total_score',data=df, palette='hls')


plt.setp(ax.get_xticklabels(), rotation=90)
plt.setp(ax.get_yticklabels(), rotation=0)

plt.show()

'''


fig, ax = plt.subplots()

# histogram our data with numpy

n, bins = np.histogram(df['total_score'], 50)

# get the corners of the rectangles for the histogram
left = np.array(bins[:-1])
right = np.array(bins[1:])
bottom = np.zeros(len(left))
top = bottom + n


# we need a (numrects x numsides x 2) numpy array for the path helper
# function to build a compound path
XY = np.array([[left, left, right, right], [bottom, top, top, bottom]]).T

# get the Path object
barpath = path.Path.make_compound_path_from_polys(XY)

# make a patch out of it
patch = patches.PathPatch(barpath)
ax.add_patch(patch)


# update the view limits
ax.set_xlim(left[0], right[-1])
ax.set_ylim(bottom.min(), top.max())
fig.set_size_inches(18.5, 10.5)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)

plt.xlabel('Number of NaNs', fontdict={'size': '26'})
plt.ylabel('Number of Games', fontdict={'size': '26'})
ax.grid(True)

plt.show()
'''