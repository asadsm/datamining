#!/usr/bin/python3

import warnings
import itertools
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pyflux as pf
import re
import sys
from sklearn.metrics import mean_squared_error, r2_score


def to_time_series(some_series):
    some_size = len(some_series)
    some_index = np.array(range(0,some_size))
    some_index = pd.to_datetime(some_index,unit='s') 
    some_data = some_series.values
    new_series = pd.DataFrame(data=some_data,index=some_index, columns=['values'])
    return new_series

def make_params(params1, params2):
    params = np.array([0.0])
    params = np.append(params,params1)
    params = np.append(params,params2)
    return params

def make_csv():
    df = pd.read_csv('toy_data/tcals_100_drop.csv',encoding='latin')
    cols = [c for c in df.columns if c.lower()[-3:] == 'max']
    max_df = df[cols]
    print("max_df, ", max_df)
    sampled_df = max_df.loc[df['type'] == 'home']
    print("sampled_df: ", sampled_df)
    sorted_0 = sampled_df.sort_values(by=['t0_max'])
    print("sorted_0, ", sorted_0)
    values = []
    for index, time_series in sorted_0.iterrows():
        print("this is values, ", values)
        time_series = time_series.dropna()
        values = np.append(values, time_series.values) 
    out_df = to_time_series(pd.Series(values)) 
    print("this is out_df: ", out_df)
    out_df.index.name = 'time'
    out_df.to_csv("time_series_out.csv")

def grid_search_model():
    df = pd.read_csv('toy_data/time_series_home.csv',encoding='latin')
    print("this is df: ", df)
    p = d = q = range(0, 4)
    pdq = list(itertools.product(p, d, q))
    min_aic = 1000000
    min_pdq = (0,0,0)
    for params in pdq:
        model = pf.ARIMA(data=df, ar=params[0], ma=params[2], integ=params[1], target='values', family=pf.Normal())
        x = model.fit("MLE")
        x.summary()
        if(x.aic < min_aic):
            min_aic = x.aic
            min_pdq = params
     
    print("min_aic: ", min_aic) 
    print("min_pdq: ", min_pdq) 

def train_model():
    df = pd.read_csv('toy_data/time_series_home.csv',encoding='latin')
    #print("this is df: ", df)

    model = pf.ARIMA(data=df, ar=3, ma=2, integ=1, target='values', family=pf.Normal())
    x = model.fit("MLE")
    x.summary()
    mu,Y=model._model(model.latent_variables.get_z_values())
    predictions=model.link(mu)
    print("predictions: ", predictions)
    print("values: ", df['values'].values[2:])
    print("Mean squared error: " , mean_squared_error(df['values'].values[4:], predictions))
    #model.plot_fit()


#get only max dataframe
#make_csv()
train_model()
sys.exit()
df = pd.read_csv('toy_data/tcals_100_drop.csv',encoding='latin')
df = df.dropna()
cols = [c for c in df.columns if c.lower()[-3:] == 'max']
max_df = df[cols]


#get my first max time series
max_time_series = max_df.loc[0]
max_time_series = to_time_series(max_time_series)

#data = pd.read_csv('https://vincentarelbundock.github.io/Rdatasets/csv/datasets/sunspot.year.csv')
#data.index = data['time'].values

#print(type(data))
#print(data['value'])
#model = pf.ARIMA(data=data, ar=4, ma=4, target='value', family=pf.Normal())
#x = model.fit("MLE")
#x.summary()

model = pf.ARIMA(data=max_time_series, ar=4, ma=4, target='values', family=pf.Normal())
x = model.fit("MLE")
x.summary()
print(x.z)
print(x.z_values)
new_model = pf.ARIMA(data=max_time_series, ar=4, ma=4, target='values', family=pf.Normal())
new_model.latent_variables.set_z_values(x.z_values,method="MLE")
print(new_model.latent_variables.get_z_values())

predictions = new_model.predict(10)
print(max_time_series)
print(predictions)
