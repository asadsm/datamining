# PRE-PROCESSING
#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib as mpl


############################ for each match_id, type, and time step, calculate the mean, max, and min odds

#df_ = pd.read_csv('all_series_odds_modified.csv', sep=',', quotechar='"', encoding="latin-1")

d1 = pd.read_csv('odds_series_29500_end_out.csv', sep=',', quotechar='"', encoding="latin-1")

d2 = pd.read_csv('odds_series_25300_29500_out.csv', sep=',', quotechar='"', encoding="latin-1")

d3 = pd.read_csv('odds_series_8500_12700_out.csv', sep=',', quotechar='"', encoding="latin-1")

d4 = pd.read_csv('odds_series_100_out.csv', sep=',', quotechar='"', encoding="latin-1")

d5 = pd.read_csv('odds_series_100_4300_out.csv', sep=',', quotechar='"', encoding="latin-1")

d6 = pd.read_csv('2odds_series_16800_21000_out.csv', sep=',', quotechar='"', encoding="latin-1")

d7 = pd.read_csv('2odds_series_12600_16800_out.csv', sep=',', quotechar='"', encoding="latin-1")

d8 = pd.read_csv('2odds_series_8400_12600_out.csv', sep=',', quotechar='"', encoding="latin-1")

d9 = pd.read_csv('2odds_series_4200_8400_out.csv', sep=',', quotechar='"', encoding="latin-1")

#####################################

d11 = pd.read_csv('odds_series_u21_4300_8500_out.csv', sep=',', quotechar='"', encoding="latin-1")

d12 = pd.read_csv('2odds_series_116900_21100_out.csv', sep=',', quotechar='"', encoding="latin-1")

d13 = pd.read_csv('odds_series_21100_253000_out.csv', sep=',', quotechar='"', encoding="latin-1")

d14 = pd.read_csv('odds_series_21100_25300_out_final.csv', sep=',', quotechar='"', encoding="latin-1")

d15 = pd.read_csv('odds_series_16900_21100_out.csv', sep=',', quotechar='"', encoding="latin-1")

d16 = pd.read_csv('odds_series_12700_16900_out.csv', sep=',', quotechar='"', encoding="latin-1")

d17 = pd.read_csv('2odds_series_u21_58800_61372_out.csv', sep=',', quotechar='"', encoding="latin-1")

d18 = pd.read_csv('2odds_series_u21_54600_58800_out.csv', sep=',', quotechar='"', encoding="latin-1")

d19 = pd.read_csv('2odds_series_u21_50400_54600_out.csv', sep=',', quotechar='"', encoding="latin-1")

d20 = pd.read_csv('2odds_series_u21_46200_50400_out.csv', sep=',', quotechar='"', encoding="latin-1")

d21 = pd.read_csv('2odds_series_u21_37800_42000_out.csv', sep=',', quotechar='"', encoding="latin-1")

d22 = pd.read_csv('2odds_series_u21_33600_378000_out.csv', sep=',', quotechar='"', encoding="latin-1")

d23 = pd.read_csv('2odds_series_u21_33600_37800_out.csv', sep=',', quotechar='"', encoding="latin-1")

d24 = pd.read_csv('2odds_series_u21_29400_33600_out.csv', sep=',', quotechar='"', encoding="latin-1")

d25 = pd.read_csv('2odds_series_u21_25200_29400_out.csv', sep=',', quotechar='"', encoding="latin-1")


#####################################

d30 = pd.read_csv('2odds_series_21000_25200_out.csv', sep=',', quotechar='"', encoding="latin-1")

d31 = pd.read_csv('2odds_series_42000_46200_out.csv', sep=',', quotechar='"', encoding="latin-1")

d32 = pd.read_csv('2odds_series_start_4200_out.csv', sep=',', quotechar='"', encoding="latin-1")



df_ = pd.concat([d1, d2, d3, d4, d5, d6, d7, d8, d9,
	d11, d12, d13, d14, d15, d16, d17, d18, d19, d20,
	d21, d22, d23, d24, d25, 
	d30, d31, d32])

# print (df_.iloc[[0,-1]])
# print (df_.shape)


############################# rank bookies based on number of NaNs (highest rank = least # of NaNs)
df_['sum_nan']=df_.isnull().sum(axis=1)



#per row, count the number of NaNs 
df_['sum_nan']=df_.isnull().sum(axis=1)

# per bookie, count the number of NaNs 
df_['sum_nan_bookie'] = df_.groupby('bookie', sort=False)["sum_nan"].transform('sum')



#rank bookies based on sum_nan_bookie (how much each bookie is playing)
df_['playing_rank'] = df_['sum_nan_bookie'].rank(method='dense', ascending=True)


df_['sum_nan_match'] = df_.groupby(['match_id', 'bookie'], sort=False)["sum_nan"].transform('sum')



############################# remove rows that have all NaNs for a match
####### 72 x 3 (draw, away, home) = 216
df_ = df_.loc[df_['sum_nan_match'] != 216]

#print (df_.dtypes["t71"])

# # #data_final.to_csv("all_series_odds_modified_toy.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")


############################# change data type

for i in range (0, 72):
	df_["t" + str(i) ]=pd.to_numeric(df_["t" + str(i) ], errors='coerce')

# df_["t0"] = pd.to_numeric(df_.t0, errors='coerce')


# print (df_.dtypes["t0"])
# print (df_.dtypes["t40"])
# print (df_.dtypes["t70"])

############################# calc t_avg, min, max
for i in range (0, 72):

	df_["t" + str(i) + "_avg"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('mean')
	df_["t" + str(i) + "_min"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('min')
	df_["t" + str(i) + "_max"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('max')

df_.to_csv("odds_series_tcalcs_final.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")
