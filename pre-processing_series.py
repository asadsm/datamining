# PRE-PROCESSING
#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib as mpl

# ############# PRE-PROCESSING STEPS: ######################


df_ = pd.read_csv('odds_series_100_4300_out.csv', sep=',', quotechar='"', encoding="latin-1")


# per row, count the number of NaNs 
df_['sum_nan']=df_.isnull().sum(axis=1)

# per bookie, count the number of NaNs 
df_['sum_nan_bookie'] = df_.groupby('bookie', sort=False)["sum_nan"].transform('sum')



#rank bookies based on sum_nan_bookie (how much each bookie is playing)
df_['playing_rank'] = df_['sum_nan_bookie'].rank(method='dense', ascending=True)


df_['sum_nan_match'] = df_.groupby(['match_id', 'bookie'], sort=False)["sum_nan"].transform('sum')



############################# remove rows that have all NaNs for a match
df_ = df_.loc[df_['sum_nan_match'] != 216]


############################ for each match_id, type, and time step, calculate the mean, max, and min odds
#df_ = pd.read_csv('all_series_odds_modified.csv', sep=',', quotechar='"', encoding="latin-1")
#print (df_.dtypes["t0"])

for i in range (0,72):

	df_["t" + str(i) + "_avg"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('mean')
	df_["t" + str(i) + "_min"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('min')
	df_["t" + str(i) + "_max"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('max')

df_.to_csv("odds_series_100_4300_tcalcs_output.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")


############################# rank bookies based on number of NaNs (highest rank = least # of NaNs)
