import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('data/closing_odds.csv.gz', compression='gzip', sep=',', quotechar='"')
outlier_data = pd.read_csv('data/closing_odds_outliers.csv', sep=',')[62073:]
outlier_data.reset_index(drop=True, inplace=True)

prior_data = pd.read_csv('data/log_reg_posteriors.csv', sep=',')

print ('Length of outlier data is ' + str(len(outlier_data)))
print ('Length of prior data is ' + str(len(prior_data)))

# Fields:
# 1. match_table_id: unique identifier of the game
# 2. league of the game
# 3. match date
# 4. home team
# 5. 90-minute score of home team
# 6. away team
# 7. 90-minute score of away team
# 8. average closing odds home win
# 9. average closing odds draw
# 10. average closing odds away win
# 11. maximum offered closing odds home win
# 12. maximum offered closing odds draw
# 13. maximum offered closing odds away win
# 14. name of bookmaker offering maximum closing odds for home win
# 15. name of bookmaker offering maximum closing odds for draw
# 16. name of bookmaker offering maximum closing odds for away win
# 17. number of available closing odds for home win
# 18. number of available closing odds for draw
# 19. number of available closing odds for away win

n_games = data.shape[0]

[u'match_id', u'league', u'match_date', u'home_team', u'home_score',
       u'away_team', u'away_score', u'avg_odds_home_win', u'avg_odds_draw',
       u'avg_odds_away_win', u'max_odds_home_win', u'max_odds_draw',
       u'max_odds_away_win', u'top_bookie_home_win', u'top_bookie_draw',
       u'top_bookie_away_win', u'n_odds_home_win', u'n_odds_draw',
       u'n_odds_away_win']

leagues = data['league']
n_leagues = pd.unique(data['league']).shape[0]

prior_home = float(sum(data['home_score'] > data['away_score'])) / n_games
prior_draw = float(sum(data['home_score'] == data['away_score'])) / n_games
prior_away = float(sum(data['home_score'] < data['away_score'])) / n_games


# Calculate accuracy of prediction as a function of the implicit probability
# contained in the odds
odds_bins = np.arange(0, 1, 0.0125)  # probability bins
min_games = 100;

# Home victory
p_home = 1 / data['avg_odds_home_win']
p_draw = 1 / data['avg_odds_draw']
p_away = 1 / data['avg_odds_away_win']

home_score = data['home_score']
away_score = data['away_score']

acc_home = []
acc_draw = []
acc_away = []

bin_odds_home_mean = []
bin_odds_draw_mean = []
bin_odds_away_mean = []

for bn in range(0, len(odds_bins) - 2):
    # Get the data from the bin
    inds_home = np.where((p_home > odds_bins[bn]) & (p_home <= odds_bins[bn + 1]))[0]
    inds_draw = np.where((p_draw > odds_bins[bn]) & (p_draw <= odds_bins[bn + 1]))[0]
    inds_away = np.where((p_away > odds_bins[bn]) & (p_away <= odds_bins[bn + 1]))[0]
    # Get accuracy for home, draw away
    if (len(inds_home) >= min_games):
        acc_home.append(float(sum(home_score[inds_home] > away_score[inds_home])) / len(inds_home))
        bin_odds_home_mean.append(np.mean(p_home[inds_home]));
    if (len(inds_draw) >= min_games):
        acc_draw.append(float(sum(home_score[inds_draw] == away_score[inds_draw])) / len(inds_draw))
        bin_odds_draw_mean.append(np.mean(p_draw[inds_draw]))
    if (len(inds_away) >= min_games):
        acc_away.append(float(sum(home_score[inds_away] < away_score[inds_away])) / len(inds_away))
        bin_odds_away_mean.append(np.mean(p_away[inds_away]))


bet = 50  # money on each bet
n_samples = 100  # number of returns to calculate (with replacement) for the random strategy
runStrategies = 1  # 1: run both strategies, 0: load results from disk


def beatthebookie_strategy_with_prior(data, prior=None, type=None):

    # data = data.sample(frac=1)

    # 0 is home win
    # 1 is away win
    # 2 is draw
    # 4-6 is avg odds (home,away,draw)
    # 7-9 is max odds (home,away,draw)
    result = data['0']
    marg = 0.1
    if prior is None:
        earn_margin_home = ((1 / data['4'] - marg) * data['7']) - 1
        earn_margin_away = ((1 / data['5'] - marg) * data['8']) - 1
        earn_margin_draw = ((1 / data['6'] - marg) * data['9']) - 1
    else:
        earn_margin_home = ((prior['home_win'] / data['4']) * data['7']) - 1
        earn_margin_away = ((prior['away_win'] / data['5']) * data['8']) - 1
        earn_margin_draw = ((prior['draw'] / data['6']) * data['9']) - 1

    max_margin = np.max(pd.concat([earn_margin_home, earn_margin_draw, earn_margin_away], axis=1), axis=1)
    max_arg = pd.concat([earn_margin_home, earn_margin_draw, earn_margin_away], axis=1).apply(np.argmax, axis=1)
    max_margin_max_odd = (max_arg == 0) * data['7'] + \
                         (max_arg == 1) * data['8'] + \
                         (max_arg == 2) * data['9']

    should_bet = max_margin > 0.0

    if type == 'attenuated':
        my_bet = 50 * max_margin
    else:
        my_bet = 50

    bets_outcome = my_bet * (max_margin_max_odd - 1) * (max_arg == result) - my_bet * (max_arg != result)

    total_money_spent = np.where(should_bet, my_bet, 0).sum()

    accuracy = (max_arg == result)[should_bet].apply(int)

    return [np.cumsum(bets_outcome[should_bet]), accuracy, total_money_spent]


def beatthebookie_strategy(data, bet=50, type=None):

    marg = 0.1  # margin odds above the mean.

    nValidOdds = 3

    result = 0 * (data['home_score'] > data['away_score']) \
             + 1 * (data['home_score'] == data['away_score']) \
             + 2 * (data['home_score'] < data['away_score'])

    earn_margin_home = ((1 / data['avg_odds_home_win'] - marg) * data['max_odds_home_win'] - 1) * \
                       (data['n_odds_home_win'] > nValidOdds)
    earn_margin_draw = ((1 / data['avg_odds_draw'] - marg) * data['max_odds_draw'] - 1) * \
                       (data['n_odds_draw'] > nValidOdds)
    earn_margin_away = ((1 / data['avg_odds_away_win'] - marg) * data['max_odds_away_win'] - 1) * \
                       (data['n_odds_away_win'] > nValidOdds)

    max_margin = np.max(pd.concat([earn_margin_home, earn_margin_draw, earn_margin_away], axis=1), axis=1)
    max_arg = pd.concat([earn_margin_home, earn_margin_draw, earn_margin_away], axis=1).apply(np.argmax, axis=1)
    max_margin_max_odd = (max_arg == 0) * data['max_odds_home_win'] + \
                         (max_arg == 1) * data['max_odds_draw'] + \
                         (max_arg == 2) * data['max_odds_away_win']

    should_bet = max_margin > 0

    if type is None:
        should_bet = max_margin > 0
        bets_outcome = bet * (max_margin_max_odd - 1) * (max_arg == result) - bet * (max_arg != result)
        total_money_spent = np.where(should_bet, bet, 0).sum()

        accuracy = (max_arg == result)[should_bet].apply(int)

        return [np.cumsum(bets_outcome[should_bet]), accuracy, total_money_spent]

    if type == 'attenuated':
        my_bet = 50 * max_margin
        bets_outcome = my_bet * (max_margin_max_odd - 1) * (max_arg == result) - my_bet * (max_arg != result)
        total_money_spent = np.where(should_bet, my_bet, 0).sum()

        accuracy = (max_arg == result)[should_bet].apply(int)

        return [np.cumsum(bets_outcome[should_bet]), accuracy, total_money_spent]


[base_money_made, base_accuracy, base_money_spent] = beatthebookie_strategy(data)
print('Beat The Bookie (Original Kernel - no changes) statistics:\n');
print(' # of bets: %2.0f \n Return: %2.4f\n Profit: %2.0f\n Money Spent: %2.0f\n' % (
base_money_made.shape[0],
np.array(base_money_made)[-1] / (base_money_made.shape[0] * bet) * 100, np.array(base_money_made)[-1], (base_money_made.shape[0] * bet)))


[attenuated_money_made, attenuated_accuracy, attenuated_money_spent] = beatthebookie_strategy(data, type='attenuated')
print('Beat The Bookie (Original Kernel - attenuated bets) statistics:\n');
print(' # of bets: %2.0f \n Return: %2.4f\n Profit: %2.0f\n Money Spent: %2.0f\n' % (
attenuated_money_made.shape[0],
np.array(attenuated_money_made)[-1] / attenuated_money_spent * 100, np.array(attenuated_money_made)[-1], attenuated_money_spent))


[outlier_money_made, outlier_accuracy, outlier_money_spent] = beatthebookie_strategy_with_prior(outlier_data)
print('Beat The Bookie (Original Kernel - Outlier Data) statistics:\n');
print(' # of bets: %2.0f \n Return: %2.4f\n Profit: %2.0f\n Money Spent: %2.0f\n' % (
outlier_money_made.shape[0],
np.array(outlier_money_made)[-1] / outlier_money_spent * 100, np.array(outlier_money_made)[-1], outlier_money_spent))


[outlier_attenuated_money_made, outlier_attenuated_accuracy, outlier_attenuated_money_spent] = beatthebookie_strategy_with_prior(outlier_data, type='attenuated')
print('Beat The Bookie (Original Kernel - Outlier Data w/ attenuated bet) statistics:\n');
print(' # of bets: %2.0f \n Return: %2.4f\n Profit: %2.0f\n Money Spent: %2.0f\n' % (
outlier_attenuated_money_made.shape[0],
np.array(outlier_attenuated_money_made)[-1] / outlier_attenuated_money_spent * 100, np.array(outlier_attenuated_money_made)[-1], outlier_attenuated_money_spent))


[outlier_prior_money_made, outlier_prior_accuracy, outlier_prior_money_spent] = beatthebookie_strategy_with_prior(outlier_data, prior=prior_data)
print('Beat The Bookie (Original Kernel - Outlier Data w/ prior) statistics:\n');
print(' # of bets: %2.0f \n Return: %2.4f\n Profit: %2.0f\n Money Spent: %2.0f\n' % (
outlier_prior_money_made.shape[0],
np.array(outlier_prior_money_made)[-1] / outlier_prior_money_spent * 100, np.array(outlier_prior_money_made)[-1], outlier_prior_money_spent))




[outlier_attenuated_prior_money_made, outlier_prior_attenuated_accuracy, outlier_attenuated_prior_money_spent] = beatthebookie_strategy_with_prior(outlier_data, type='attenuated', prior=prior_data)
print('Beat The Bookie (Original Kernel - Outlier Data w/ prior + attenuated bet) statistics:\n');
print(' # of bets: %2.0f \n Return: %2.4f\n Profit: %2.0f\n Money Spent: %2.0f\n' % (
outlier_attenuated_prior_money_made.shape[0],
np.array(outlier_attenuated_prior_money_made)[-1] / outlier_attenuated_prior_money_spent * 100, np.array(outlier_attenuated_prior_money_made)[-1], outlier_attenuated_prior_money_spent))


percent_gain_base = base_money_made / base_money_spent
percent_gain_base_attenuated = attenuated_money_made / attenuated_money_spent
percent_gain_base_outlier = outlier_money_made / outlier_money_spent
percent_gain_outlier_attenuated = outlier_attenuated_money_made / outlier_attenuated_money_spent
percent_gain_outlier_prior = outlier_prior_money_made / outlier_prior_money_spent
percent_gain_outlier_prior_attenuated = outlier_attenuated_prior_money_made / outlier_attenuated_prior_money_spent


old_x_axis = np.arange(len(percent_gain_base))
outlier_x_axis = np.arange(len(percent_gain_base_outlier))
base_line, = plt.plot(np.arange(percent_gain_base.shape[0]), percent_gain_base, label='Original Kernel')
base_attenuated_line, = plt.plot(np.arange(percent_gain_base_attenuated.shape[0]), percent_gain_base_attenuated, label='Attenuated Kernel')
base_outlier_line, = plt.plot(np.arange(percent_gain_base_outlier.shape[0]), percent_gain_base_outlier, label='Outlier Kernel')
base_outlier_attenuated_line, = plt.plot(np.arange(percent_gain_outlier_attenuated.shape[0]), percent_gain_outlier_attenuated, label='Attenuated Outlier Kernel')
base_outlier_prior_line, = plt.plot(np.arange(percent_gain_outlier_prior.shape[0]), percent_gain_outlier_prior, label='Prior Outlier Kernel')
base_outlier_attenuated_prior_line, = plt.plot(np.arange(percent_gain_outlier_prior_attenuated.shape[0]), percent_gain_outlier_prior_attenuated, label='sAttenuated Prior Outlier Kernel')

plt.legend([base_line, base_attenuated_line, base_outlier_line,
            base_outlier_attenuated_line, base_outlier_prior_line, base_outlier_attenuated_prior_line], ['Original Kernel', 'Attenuated Kernel',
                                                  'Outlier Kernel', 'Attenuated Outlier Kernel',
                                                'Prior Outlier Kernel', 'Attenuated Prior Outlier'])


plt.ylabel("Gain")
plt.xlabel("Number of bets")
plt.show()