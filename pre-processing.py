# PRE-PROCESSING
#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib as mpl

# ############# PRE-PROCESSING STEPS: ######################
# 1) Remove U21 leagues
# 2) Create columns for game outcome type, time steps
# 3) Playing rank, remove NaNs


# odds1 = pd.read_csv("odds_series.csv", encoding="latin-1")
# #print (odds1.shape)

# meta_1a = pd.read_csv("odds_series_matches.csv", encoding="latin-1")
# #print (meta_1a[:5])

# meta1 = meta_1a[meta_1a.columns[0:3]]
# #print (meta_1[:10])
# #print (meta1.shape)
# # print (meta1[:5])

# # merge 
# data1 = pd.merge(meta1, odds1, on='match_id')
# #print (data1.shape)


# # odds series 2
# odds2 = pd.read_csv("odds_series_b.csv", encoding="latin-1")
# #print (odds2.shape)

# meta_2a = pd.read_csv("odds_series_b_matches.csv", encoding="latin-1")

# meta2 = meta_2a[meta_2a.columns[0:3]]
# #print (meta2.shape)



# data2 = pd.merge(meta2, odds2, on='match_id')
# #print (data2.shape)

# #row bind 
# data = pd.concat([data1, data2])
# #print (data.shape)

# #print (data[:5])

# ### exports
# # data_small = data[:50]
# # data_small.to_csv("all_series_toy.csv", sep=',', quotechar = '"', na_rep='nan', index=False)

# # data.to_csv("all_series_total.csv", sep=',', quotechar = '"', na_rep='nan', index=False)


# ###################### remove U21
# data_rev = data.loc[data[' league'].str.contains("U21") == False]

#data_rev.to_csv("all_series_odds.csv", sep=',', quotechar = '"', na_rep='nan', encoding="latin-1", index=False)

# data_rev = data_rev[:10]
# data_rev.to_csv("all_series_toy.csv", sep=',', quotechar = '"', na_rep='nan', encoding="latin-1", index=False)



#print (data_rev.shape)



# ##################### create time step columns
read_data = pd.read_csv('all_series_odds.csv', sep=',', quotechar='"', encoding="latin-1")

temp_data = read_data[:1000]
#temp_data = data_rev
df_ = temp_data[['match_id','match_date','match_time','score_home','score_away']].copy()
df_['league'] = temp_data[' league']
df_['home_team'] = temp_data[' home_team']
df_ = pd.concat([df_, pd.DataFrame(columns=['bookie','type','t0','t1','t2','t3','t4','t5','t6','t7','t8','t9','t10',
                                            't11','t12','t13','t14','t15','t16','t17','t18','t19','t20',
                           't21','t22','t23','t24','t25','t26','t27','t28','t29','t30','t31','t32','t33','t34','t35','t36','t37','t38','t39','t40',
                           't41','t42','t43','t44','t45','t46','t47','t48','t49','t50','t51','t52','t53','t54','t55','t56','t57','t58','t59','t60',
                           't61','t62','t63','t64','t65','t66','t67','t68','t69','t70', 't71'])])
df_ = df_.fillna('*')

def replicate_bookie_func(group, n=33):
    return pd.DataFrame(dict(match_id=np.repeat(group.match_id.values, n-1), league=np.repeat(group.league.values, n-1), home_team=np.repeat(group.home_team.values, n-1), match_date=np.repeat(group.match_date.values,n-1), type=np.repeat(group.type.values,n-1), match_time=np.repeat(group.match_time.values,n-1), score_home=np.repeat(group.score_home.values,n-1), score_away=np.repeat(group.score_away.values,n-1), 
                             t0=np.repeat(group.t0.values, n-1), t1=np.repeat(group.t1.values, n-1), t2=np.repeat(group.t2.values, n-1), t3=np.repeat(group.t3.values, n-1), t4=np.repeat(group.t4.values, n-1), t5=np.repeat(group.t5.values, n-1), t6=np.repeat(group.t6.values, n-1), t7=np.repeat(group.t7.values, n-1), t8=np.repeat(group.t8.values, n-1), t9=np.repeat(group.t9.values, n-1), t10=np.repeat(group.t10.values, n-1),
                             t11=np.repeat(group.t11.values, n-1), t12=np.repeat(group.t12.values, n-1), t13=np.repeat(group.t13.values, n-1), t14=np.repeat(group.t14.values, n-1), t15=np.repeat(group.t15.values, n-1), t16=np.repeat(group.t16.values, n-1), t17=np.repeat(group.t17.values, n-1), t18=np.repeat(group.t18.values, n-1), t19=np.repeat(group.t19.values, n-1), t20=np.repeat(group.t20.values, n-1), 
                             t21=np.repeat(group.t21.values, n-1), t22=np.repeat(group.t22.values, n-1), t23=np.repeat(group.t23.values, n-1), t24=np.repeat(group.t24.values, n-1), t25=np.repeat(group.t25.values, n-1), t26=np.repeat(group.t26.values, n-1), t27=np.repeat(group.t27.values, n-1), t28=np.repeat(group.t28.values, n-1), t29=np.repeat(group.t29.values, n-1), t30=np.repeat(group.t30.values, n-1), 
                             t31=np.repeat(group.t31.values, n-1), t32=np.repeat(group.t32.values, n-1), t33=np.repeat(group.t33.values, n-1), t34=np.repeat(group.t34.values, n-1), t35=np.repeat(group.t35.values, n-1), t36=np.repeat(group.t36.values, n-1), t37=np.repeat(group.t37.values, n-1), t38=np.repeat(group.t38.values, n-1), t39=np.repeat(group.t39.values, n-1), t40=np.repeat(group.t40.values, n-1), 
                             t41=np.repeat(group.t41.values, n-1), t42=np.repeat(group.t42.values, n-1), t43=np.repeat(group.t43.values, n-1), t44=np.repeat(group.t44.values, n-1), t45=np.repeat(group.t45.values, n-1), t46=np.repeat(group.t46.values, n-1), t47=np.repeat(group.t47.values, n-1), t48=np.repeat(group.t48.values, n-1), t49=np.repeat(group.t49.values, n-1), t50=np.repeat(group.t50.values, n-1), 
                             t51=np.repeat(group.t51.values, n-1), t52=np.repeat(group.t52.values, n-1), t53=np.repeat(group.t53.values, n-1), t54=np.repeat(group.t54.values, n-1), t55=np.repeat(group.t55.values, n-1), t56=np.repeat(group.t56.values, n-1), t57=np.repeat(group.t57.values, n-1), t58=np.repeat(group.t58.values, n-1), t59=np.repeat(group.t59.values, n-1), t60=np.repeat(group.t60.values, n-1), 
                             t61=np.repeat(group.t61.values, n-1), t62=np.repeat(group.t62.values, n-1), t63=np.repeat(group.t63.values, n-1), t64=np.repeat(group.t64.values, n-1), t65=np.repeat(group.t65.values, n-1), t66=np.repeat(group.t66.values, n-1), t67=np.repeat(group.t67.values, n-1), t68=np.repeat(group.t68.values, n-1), t69=np.repeat(group.t69.values, n-1), t70=np.repeat(group.t70.values, n-1), 
                             t71=np.repeat(group.t71.values, n-1), bookie=np.arange(1,n)))

df_ = df_.groupby(level=0).apply(replicate_bookie_func).reset_index(drop=True)

def replicate_type_func(group, n=3):
    return pd.DataFrame(dict(match_id=np.repeat(group.match_id.values, n), league=np.repeat(group.league.values, n), home_team=np.repeat(group.home_team.values, n), match_date=np.repeat(group.match_date.values,n), match_time=np.repeat(group.match_time.values,n), score_home=np.repeat(group.score_home.values,n), score_away=np.repeat(group.score_away.values,n), 
                             t0=np.repeat(group.t0.values, n), t1=np.repeat(group.t1.values, n), t2=np.repeat(group.t2.values, n), t3=np.repeat(group.t3.values, n), t4=np.repeat(group.t4.values, n), t5=np.repeat(group.t5.values, n), t6=np.repeat(group.t6.values, n), t7=np.repeat(group.t7.values, n), t8=np.repeat(group.t8.values, n), t9=np.repeat(group.t9.values, n), t10=np.repeat(group.t10.values, n),
                             t11=np.repeat(group.t11.values, n), t12=np.repeat(group.t12.values, n), t13=np.repeat(group.t13.values, n), t14=np.repeat(group.t14.values, n), t15=np.repeat(group.t15.values, n), t16=np.repeat(group.t16.values, n), t17=np.repeat(group.t17.values, n), t18=np.repeat(group.t18.values, n), t19=np.repeat(group.t19.values, n), t20=np.repeat(group.t20.values, n), 
                             t21=np.repeat(group.t21.values, n), t22=np.repeat(group.t22.values, n), t23=np.repeat(group.t23.values, n), t24=np.repeat(group.t24.values, n), t25=np.repeat(group.t25.values, n), t26=np.repeat(group.t26.values, n), t27=np.repeat(group.t27.values, n), t28=np.repeat(group.t28.values, n), t29=np.repeat(group.t29.values, n), t30=np.repeat(group.t30.values, n), 
                             t31=np.repeat(group.t31.values, n), t32=np.repeat(group.t32.values, n), t33=np.repeat(group.t33.values, n), t34=np.repeat(group.t34.values, n), t35=np.repeat(group.t35.values, n), t36=np.repeat(group.t36.values, n), t37=np.repeat(group.t37.values, n), t38=np.repeat(group.t38.values, n), t39=np.repeat(group.t39.values, n), t40=np.repeat(group.t40.values, n), 
                             t41=np.repeat(group.t41.values, n), t42=np.repeat(group.t42.values, n), t43=np.repeat(group.t43.values, n), t44=np.repeat(group.t44.values, n), t45=np.repeat(group.t45.values, n), t46=np.repeat(group.t46.values, n), t47=np.repeat(group.t47.values, n), t48=np.repeat(group.t48.values, n), t49=np.repeat(group.t49.values, n), t50=np.repeat(group.t50.values, n), 
                             t51=np.repeat(group.t51.values, n), t52=np.repeat(group.t52.values, n), t53=np.repeat(group.t53.values, n), t54=np.repeat(group.t54.values, n), t55=np.repeat(group.t55.values, n), t56=np.repeat(group.t56.values, n), t57=np.repeat(group.t57.values, n), t58=np.repeat(group.t58.values, n), t59=np.repeat(group.t59.values, n), t60=np.repeat(group.t60.values, n), 
                             t61=np.repeat(group.t61.values, n), t62=np.repeat(group.t62.values, n), t63=np.repeat(group.t63.values, n), t64=np.repeat(group.t64.values, n), t65=np.repeat(group.t65.values, n), t66=np.repeat(group.t66.values, n), t67=np.repeat(group.t67.values, n), t68=np.repeat(group.t68.values, n), t69=np.repeat(group.t69.values, n), t70=np.repeat(group.t70.values, n), 
                             t71=np.repeat(group.t71.values, n), bookie=np.repeat(group.bookie.values, n), type=np.arange(n)))

df_ = df_.groupby(level=0).apply(replicate_type_func).reset_index(drop=True)

# Convert 0,1,2 of type to "home, draw, away"
data = pd.read_csv('all_series_odds.csv', sep=',', quotechar='"', index_col="match_id", encoding="latin-1")
for index, row in df_.iterrows():
    if(df_.loc[index, 'type'] == 0):
        df_.loc[index, 'type'] = "home"
    if(df_.loc[index, 'type'] == 1):
        df_.loc[index, 'type'] = "draw"
    if(df_.loc[index, 'type'] == 2):
        df_.loc[index, 'type'] = "away"
        
for index, row in data.iterrows():
    col_names = list(data)
    for col in col_names:
        if any(char.isdigit() for char in col) == True: 
            #print(index, "***", col)
            c = col.split('_')
            t = 't'+c[2]
            #print(df_[(df_['match_id'] == index) & (df_['type'] == c[0]) & (df_['bookie'] == int(c[1].split('b')[1]))].index.tolist())
            df_.loc[df_[(df_['match_id'] == index) & (df_['type'] == c[0]) & (df_['bookie'] == int(c[1].split('b')[1]))].index.tolist()[0],t] = data.at[index,col]

# To rearrange the dataframe columns
df_ = df_[['match_id', 'match_date', 'match_time', 'league', 'home_team', 'score_home', 'score_away',
          'bookie','type','t0','t1','t2','t3','t4','t5','t6','t7','t8','t9','t10',
            't11','t12','t13','t14','t15','t16','t17','t18','t19','t20',
            't21','t22','t23','t24','t25','t26','t27','t28','t29','t30',
            't31','t32','t33','t34','t35','t36','t37','t38','t39','t40',
            't41','t42','t43','t44','t45','t46','t47','t48','t49','t50',
            't51','t52','t53','t54','t55','t56','t57','t58','t59','t60',
            't61','t62','t63','t64','t65','t66','t67','t68','t69','t70', 't71']]


############################ for each match_id, type, and time step, calculate the mean, max, and min odds

for i in range (0,72):

	df_["t" + str(i) + "_avg"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('mean')
	df_["t" + str(i) + "_min"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('min')
	df_["t" + str(i) + "_max"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('max')

#df_.to_csv("all_series_odds_modified.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")


############################# rank bookies based on number of NaNs (highest rank = least # of NaNs)



# per row, count the number of NaNs 
df_['sum_nan']=df_.isnull().sum(axis=1)

# per bookie, count the number of NaNs 
df_['sum_nan_bookie'] = df_.groupby('bookie', sort=False)["sum_nan"].transform('sum')



#rank bookies based on sum_nan_bookie (how much each bookie is playing)
df_['playing_rank'] = df_['sum_nan_bookie'].rank(method='dense', ascending=True)


df_['sum_nan_match'] = df_.groupby(['match_id', 'bookie'], sort=False)["sum_nan"].transform('sum')



############################# remove rows that have all NaNs for a match
####### CHANGE 33
data_final = df_.loc[data['sum_nan_match'] != 216]

data_final.to_csv("all_series_odds_modified_toy.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")
