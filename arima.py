#!/usr/bin/python3

import warnings
import itertools
import pandas as pd
import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
import re
import sys

def get_start_pdq(time_series):
        #run the model
        p = d = q = range(0, 4)

        # Generate all different combinations of p, q and q triplets
        #get best AIC
        pdq = list(itertools.product(p, d, q))
        min_pdq = (500,500,500)
        min_aic = 10000000
        for params in pdq:
            try:
                    mod = sm.tsa.ARIMA(time_series,order=params)
                    results = mod.fit()
                    print("pdq: ", params)
                    print("aic: ", results.aic)
                    if(results.aic < min_aic):
                        min_pdq = params
                        min_aic = results.aic
                    #print(results1.summary())
            except Exception as e:
                print(str(e))
                continue

        print("min_aic:", min_aic) 
        print("min_pdq:", min_pdq)
        return min_pdq

def to_time_series(some_series):
    some_size = len(some_series)
    some_index = np.array(range(0,some_size))
    some_index = pd.to_datetime(some_index,unit='s') 
    some_data = some_series.values
    new_series = pd.Series(data=some_data,index=some_index)
    return new_series

def make_params(params1, params2):
    params = np.array([0.0])
    params = np.append(params,params1)
    params = np.append(params,params2)
    return params

#get only max dataframe
df = pd.read_csv('pre_processed_data/odds_series_three_reduced.csv',encoding='latin')
df = df.dropna()
cols = [c for c in df.columns if c.lower()[-3:] == 'max']
max_df = df[cols]

#get my first max time series
max_time_series = max_df.loc[0]
max_time_series = to_time_series(max_time_series)

#train for first pdq
#min_pdq = get_start_pdq(max_time_series)

#trains first step
min_pdq = (2,0,3)
mod = sm.tsa.ARIMA(max_time_series,order=min_pdq)
results = mod.fit()
resid = results.resid
mse = (resid ** 2).mean(axis=None)
print("mse: ", mse)
#results.plot_predict()
#plt.title('Predicted Values vs Actual Values', fontsize=20)
#plt.xlabel('Time Step', fontsize=18)
#plt.ylabel('Max Odds', fontsize=16)
#plt.show()
#drop first step and set intial params
max_df = max_df.drop([0])
cur_params = make_params(results.arparams, results.maparams)
params_plot = [cur_params]
plot_x = [1]
count = 2
for index, time_series in max_df.iterrows():
    try: 
        print("cur_params: ", cur_params)
        print("results.aic: ", results.aic)
        new_time_series = to_time_series(time_series)
        mod = sm.tsa.ARIMA(new_time_series,order=min_pdq)
        results = mod.fit(start_params=cur_params)
        cur_params = make_params(results.arparams,results.maparams)
        params_plot.append(cur_params)
        plot_x.append(count)
        count = count + 1
        print("index was clean: ", index)
    except: 
        print("index took exception: ", index)
        exception = 1

resid = results.resid
mse = (resid ** 2).mean(axis=None)
print("mse full train: ", mse)
print("arima aic:", results.aic)
#results.plot_predict()
#plt.title('Predicted Values vs Actual Values', fontsize=20)
#plt.xlabel('Time Step', fontsize=18)
#plt.ylabel('Max Odds', fontsize=16)
#plt.show()
#plt.plot(plot_x,params_plot)
#plt.title('Parameter Updates', fontsize=20)
#plt.xlabel('Odds Series Number', fontsize=18)
#plt.ylabel('Parameter Values', fontsize=16)
#plt.show()
