# PRE-PROCESSING
#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib as mpl



# df_ = pd.read_csv('all_series_odds_modified.csv', sep=',', quotechar='"', encoding="latin-1")

df_ = pd.read_csv('odds_series_timesteps_final_removeU.csv', sep=',', quotechar='"', encoding="latin-1")

#print (df_.shape)


############################ remove under 21 leagues via home_team
# df_ = df_.loc[df_['home_team'].str.contains("U" + "1") == False]
# df_ = df_.loc[df_['home_team'].str.contains("U" + "2") == False]
# df_ = df_.loc[df_['home_team'].str.contains("Junior") == False]

#print (df_.shape)

############################# rank bookies based on number of NaNs (highest rank = least # of NaNs)
df_['sum_nan']=df_.isnull().sum(axis=1)



#per row, count the number of NaNs 
df_['sum_nan']=df_.isnull().sum(axis=1)

# per bookie, count the number of NaNs 
df_['sum_nan_bookie'] = df_.groupby('bookie', sort=False)["sum_nan"].transform('sum')



#rank bookies based on sum_nan_bookie (how much each bookie is playing)
df_['playing_rank'] = df_['sum_nan_bookie'].rank(method='dense', ascending=True)


df_['sum_nan_match'] = df_.groupby(['match_id', 'bookie'], sort=False)["sum_nan"].transform('sum')



############################# remove rows that have all NaNs for a match
####### 72 x 3 (draw, away, home) = 216
df_ = df_.loc[df_['sum_nan_match'] != 216]

print (df_.shape)

df_.to_csv("odds_series_timesteps_final_ru_rnans_br.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")


# #data_final.to_csv("all_series_odds_modified_toy.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")


# ############################# change data type

# for i in range (0, 72):
# 	df_["t" + str(i) ]=pd.to_numeric(df_["t" + str(i) ], errors='coerce')

# # df_["t0"] = pd.to_numeric(df_.t0, errors='coerce')


# # print (df_.dtypes["t0"])
# # print (df_.dtypes["t40"])
# # print (df_.dtypes["t70"])

# ############################# calc t_avg, min, max
# for i in range (0, 72):

# 	df_["t" + str(i) + "_avg"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('mean')
# 	df_["t" + str(i) + "_min"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('min')
# 	df_["t" + str(i) + "_max"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('max')

# df_.to_csv("odds_series_tcalcs_final.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")
