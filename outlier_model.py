#!/usr/bin/python3

import numpy as np
import pandas as pd
from math import isnan
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import mean_squared_error, accuracy_score, roc_auc_score, roc_curve, auc
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import label_binarize

data = pd.read_csv('preproc.csv', sep=',', quotechar='"', encoding='latin-1')
#data = pd.read_csv('out_preproc.csv', sep=',', quotechar='"', encoding='latin-1', chunksize=10**3)
#data = next(data)

n_games = data.shape[0]
print('Total number of games: ' + str(n_games) + "\n")

bookies_outliers = np.zeros(32, dtype=float)
bookies_outliers_agree = np.zeros(32, dtype=float)
bookies_outliers_disagree = np.zeros(32, dtype=float)

def train_outlier(row):
  bookies_outlier = np.zeros(32, dtype=float)

  home_score = row['score_home']
  away_score = row['score_away']

  result = (0 * (home_score > away_score) +
            1 * (home_score < away_score) +
            2 * (home_score == away_score))

  home_odds = []
  away_odds = []
  draw_odds = []

  for bookie in range(1,33):
    home_odd = row['home_b%d_71' % bookie]
    away_odd = row['away_b%d_71' % bookie]
    draw_odd = row['draw_b%d_71' % bookie]

    if not isnan(home_odd) and not isnan(away_odd) and not isnan(draw_odd):
      home_odds.append(home_odd)
      away_odds.append(away_odd)
      draw_odds.append(draw_odd)

  std_dev = np.std(home_odds), np.std(away_odds), np.std(draw_odds)
  mean = np.mean(home_odds), np.mean(away_odds), np.mean(draw_odds)
  try:
    max_odds = np.max(home_odds), np.max(away_odds), np.max(draw_odds)
    min_odds = np.min(home_odds), np.min(away_odds), np.min(draw_odds)
  except:
    max_odds = 0,0,0
    min_odds = 0,0,0

  for bookie in range(1,33):
    home_odd = row['home_b%d_71' % bookie]
    away_odd = row['away_b%d_71' % bookie]
    draw_odd = row['draw_b%d_71' % bookie]

    if abs(home_odd - mean[0]) > 2 * std_dev[0] or \
       abs(away_odd - mean[1]) > 2 * std_dev[1] or \
       abs(draw_odd - mean[2]) > 2 * std_dev[2]:
      bookies_outlier[bookie - 1] = 1
  
  new_data = [std_dev, mean, max_odds, min_odds, bookies_outlier]
  new_data = [item for sublist in new_data for item in sublist]
  return np.array(new_data)

def home_win_lose(row):
	home_score = row['score_home']
	away_score = row['score_away']
	return(0 * (home_score > away_score) +
		1 * (home_score < away_score) +
		2 * (home_score == away_score)) 

# Outlier
print("Preprocessing data...")

# Extract y values
y = data[data.columns[1]]
data = data.drop(data.columns[1], axis=1)
data = data.replace('nan', 0)

#old_length = len(data)
#data = data.loc[data['score_away'] < 30].loc[data['score_home'] < 30]
#print('Dropped ' + str(old_length - len(data)) + ' as score outliers')

# Separate data into input and output
#X = processed_data.drop(['home_win', 'score_home', 'score_away', 'top_bookie_home_win', 'top_bookie_draw', 'top_bookie_away_win', 'home_team', 'away_team', 'league', 'match_date'], axis=1)

X_train, X_test, y_train, y_test = train_test_split(data, y, test_size=0.33, random_state=42)

print(X_train.shape)
print(y_train.shape)

print(X_test.shape)
print(y_test.shape)

y_test_bin = label_binarize(y_test, classes=[0,1,2])

def score_clf(clf_cv, name):
  print(name, "scores:")
  print("Hyper params: " + str(clf_cv.best_params_))
  print("Test set score: ", clf_cv.score(X_test, y_test))
  y_score = clf_cv.predict_proba(X_test)

  fpr = [0] * 3
  tpr = [0] * 3
  roc_auc = [0] * 3
  for i in range(3):
      fpr[i], tpr[i], _ = roc_curve(y_test_bin[:, i], y_score[:, i])
      roc_auc[i] = auc(fpr[i], tpr[i])

  with open("pre_roc_%s.out" % name, 'w') as f:
    for i in range(3):
      print(",".join([str(f) for f in fpr[i]]), file=f)
      print(",".join([str(f) for f in tpr[i]]), file=f)

  print("ROC AUC:", roc_auc)
  print()

run_log_reg = False
run_svc_lin = False
run_svc_rbf = False
run_rand_for = True

# Logistic Regression
if run_log_reg:
  log_reg_params = {
    'penalty': ['l1', 'l2'],
    'C': 10. ** np.linspace(-2,2,20)
  }

  log_reg = GridSearchCV(LogisticRegression(), log_reg_params, cv=5, verbose=2, n_jobs=-1)
  log_reg.fit(X_train, y_train)

# SVC
if run_svc_lin:
  svc_lin_params = {
    'kernel': ['linear'],
    #'C': 10. ** np.linspace(-2,2,5)
    'C': [10. ** -2]
    #'C': [10. ** -2, 10. ** -1, 1]
  }

  svc_lin = GridSearchCV(SVC(probability=True), svc_lin_params, cv=1, verbose=2, n_jobs=-1)
  svc_lin = SVC(probability=True, kernel='linear', C=10.**-2, verbose=2)
  svc_lin.fit(X_train, y_train)

# SVC RBF
if run_svc_rbf:
  svc_rbf_params = {
    'kernel': ['rbf'],
    #'C': 10. ** np.linspace(-2,2,5)
    'C': [10. ** -2]
  }

  svc_rbf = GridSearchCV(SVC(probability=True), svc_rbf_params, cv=3, verbose=2, n_jobs=-1)
  svc_rbf.fit(X_train, y_train)

if run_rand_for:
  log_reg_params = {
    'penalty': ['l1', 'l2'],
    'C': 10. ** np.linspace(-2,2,20)
  }

  log_reg = GridSearchCV(RandomForestClassifier(), rand_for_params, cv=3, verbose=2, n_jobs=-1)
  log_reg.fit(X_train, y_train)

# Scoring
if run_log_reg: score_clf(log_reg, 'log_reg')
if run_svc_lin: score_clf(svc_lin, 'svc_lin')
if run_svc_rbf: score_clf(svc_rbf, 'svc_rbf')
if run_rand_for: score_clf(rand_for, 'rand_for')



'''
tree_clf = DecisionTreeClassifier(random_state=42)
tree_clf.fit(X_train, y_train)
y_predict = tree_clf.predict(X_test)
tree_MSE = mean_squared_error(y_test, y_predict)
print("MSE of Decision Tree Model: " + str(tree_MSE))
'''
