#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
import sys

def make_csv_file():
    df = pd.read_csv('toy_data/window_100.csv',encoding='latin')
    #df = df.dropna()
    cols = [c for c in df.columns if c.lower()[-3:] == 'max']
    cols.append('match_id')
    max_windows = df[cols]
    all_rows = pd.DataFrame(columns=[0,1,2,3,4,5,6,7,8,9,'match_id','time','target'])
    #print("this is beginning all_rows: ", all_rows)
    cur_row = 20
    index = 1
    num_rows = max_windows.shape[0]
    #print("num_rows: ", num_rows)
    #print(max_windows)
    while (cur_row < num_rows):
        cur_rows = max_windows.iloc[cur_row:cur_row+10]
        col_num = 0
        #print("cur_rows: ", cur_rows)
        match_id = cur_rows['match_id'].iloc[0]
        #print("this is match_id: ", match_id)
        cur_rows = cur_rows.drop(columns=['match_id']) 
        for col in cur_rows.columns:
            if(col == "w62_max"):
                continue
            col_data = cur_rows[col]
            #print("this is col_data: ", col_data)
            #print("the isnull result: ", col_data.isnull())
            if(col_data.isnull().any().any()):
                #print("had a NAN")
                col_num = col_num + 1
                continue
            col_data = col_data.rename(index)
            #col_data = col_data.reindex(range(0,10))
            col_data = pd.Series(col_data.values, index=range(0,10))
            target_col = cur_rows.ix[:,col_num+1]
            target = target_col.iloc[9]
        
            #print("this is the target: ", target)
            if(np.isnan(target)):
                #print("target was a nan:")
                col_num = col_num + 1
                continue
            append_series = pd.Series([target,match_id,col_num],index=['target','match_id','time'])
            append_series = append_series.rename("add_series")
            #print("this is append_series: ", append_series)
            col_data = col_data.append(append_series) 
            all_rows = all_rows.append(col_data, ignore_index=True)
            index = index + 1
            col_num = col_num + 1
            #print("now this is col_data: ", col_data)
            #print("all_rows: ", all_rows)
            
        cur_row = cur_row + 30
        #print("doing da loop")
 
    all_rows.index.name = 'row'
    all_rows.to_csv('out.csv')

def make_predictions(model):
    df = pd.read_csv('toy_data/max_window_home.csv',encoding='latin')
    new_df = pd.DataFrame(columns=[0,1,2,3,4,5,6,7,8,9,'match_id','time'])
    data_df = df.drop(columns=['target','match_id','time','row'])
    data = data_df.as_matrix()
    index = 0
    for d in data:
        match_id = df.iloc[index]['match_id']
        time = df.iloc[index]['time']
        #print("this is index: ", index, " match_id: ", match_id, " time: ", time)
        new_row = d
        for i in range(0,10):
            #print("this is my new_row now: ", new_row)
            new_prediction = model.predict([new_row])[0]
            #print("this is new_prediction: ", new_prediction)
            new_row = np.delete(new_row,0)
            new_row = np.append(new_row,new_prediction)
        new_series = pd.Series([match_id, time],index=['match_id','time'])
        data_series = pd.Series(new_row, index=range(0,10))
        new_series = new_series.append(data_series)
        #print("this is my new_series to append: ", new_series)
        new_df = new_df.append(new_series, ignore_index=True)    
        index = index + 1
        #print("now this is my new_df: ", new_df)
    new_df.index.name = 'row'
    new_df.to_csv('predict.csv')
         


def train_model():
    df = pd.read_csv('toy_data/max_window_home.csv',encoding='latin')
    #print("this is original df: ", df)
     
    reg = linear_model.LinearRegression()
    target = df['target']
    #print("this is target: ", target)
    drop_df = df.drop(columns=['target','match_id','time','row'])
    #print("this is new df: ", df)
    data = drop_df.as_matrix()
    print("this is data: ", data)
    data = np.delete(data, 0,1)
    data = np.delete(data, 0,1)
    data = np.delete(data, 0,1)
    data = np.delete(data, 0,1)
    data = np.delete(data, 0,1)
    data = np.delete(data, 0,1)
    data = np.delete(data, 0,1)
    data = np.delete(data, 0,1)
    data = np.delete(data, 0,1)
    print("now this is data: ", data)
    size_div_2 = int(data.shape[0]/2)
    data_train, data_test = data[:size_div_2,:], data[size_div_2:,:]
    target_train, target_test = target[:size_div_2], target[size_div_2:]
    reg.fit(data_train,target_train)
    print("Here are coefficients: ", reg.coef_ )
    predictions = reg.predict(data_test)
    print("Mean squared error: " , mean_squared_error(target_test, predictions))
    return reg

#make_csv_file()
model = train_model()
#make_predictions(model)
sys.exit()



