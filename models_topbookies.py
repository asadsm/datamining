#!/usr/bin/python3
import numpy as np
import pandas as pd
import matplotlib as mpl
from sklearn import svm
from sklearn.model_selection import train_test_split

from sklearn.model_selection import (train_test_split,GridSearchCV)
from sklearn.metrics import (accuracy_score,roc_auc_score)
from sklearn.ensemble import (RandomForestClassifier,GradientBoostingClassifier,AdaBoostClassifier)
from sklearn import preprocessing

import matplotlib.pyplot as plt 
plt.rc("font", size=14)
import seaborn as sns
sns.set(style="white")
sns.set(style="whitegrid", color_codes=True)


from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score, accuracy_score
from sklearn.naive_bayes import GaussianNB
from sklearn import preprocessing
from sklearn.model_selection import KFold
from sklearn.linear_model import LogisticRegressionCV, LogisticRegression
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
import numpy as np
import matplotlib.pyplot as plt

from sklearn.preprocessing import label_binarize
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import mean_squared_error, accuracy_score, roc_auc_score, roc_curve, auc
from sklearn.linear_model import LogisticRegression

df = pd.read_csv("odds_series_timesteps_final_ru_rnans_br_sample_tcalcs.csv", sep=',', quotechar='"', encoding="latin-1")

#df = pd.read_csv("odds_series_timesteps_final_ru_rnans_br.csv", sep=',', quotechar='"', encoding="latin-1")

# drop last row
#df = df[:-1]

#print (df.head())
# print (df.iloc[[0,-1]])

# # (266175, 301)
# print (df.shape)

############### create "league_gender"
df['league_gender'] = 0

df.loc[df['league'].str.contains('Women'), 'league_gender'] = 1

# print (df.iloc[[0,-1]])

# subset = df.loc[df['league_gender'] == 1]


############# 
df = df.fillna(0)

#subset.to_csv("league_gender.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")


############### create "results" column
# 0 = draw
# 1 = home win
# 2 = away win

df.loc[df['score_home'] == df['score_away'], 'result'] = 0
df.loc[df['score_home'] > df['score_away'], 'result'] = 1
df.loc[df['score_home'] < df['score_away'], 'result'] = 2

# barplot for y
# sns.countplot(x='result',data=df, palette='hls')
#plt.show()

################ 
#df = df[['match_id', 'bookie', 'type', 'playing_rank','t70_avg']]

df = df[['result', 'type', 'playing_rank', 't67_avg', 't68_avg', 't69_avg', 't70_avg', 't71_avg', 'league_gender']]


# print (df.iloc[[0,-1]])
# print (df.shape)


#df[['"t70', 't71"']] = df[['t70', 't71']].astype(float)

# df[['t70','t71']] = df[['t70','t71']].apply(pd.to_numeric)

#print (df.t71.dtype)

# check the missing values
# print (df.isnull().sum())

# sns.countplot(x='playing_rank',data=df, palette='hls')
# plt.show()

## create dummy variables
data2 = pd.get_dummies(df, columns =['type', 'league_gender'])




#print (data2.head())

#### correlation plot
# sns.heatmap(data2.corr())
# plt.show()

#### split data
X = data2.iloc[:,1:]
y = data2.iloc[:,0]

# print (X.head())
# print (y.head())

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.33, random_state=42)

# print (X_train.shape)
# print (y_train.shape)

# print (X_test.shape)
# print (y_test.shape)

y_test_bin = label_binarize(y_test, classes=[0,1,2])

def score_clf(clf_cv, name):
  print(name, "scores:")
  print("Hyper params: " + str(clf_cv.best_params_))
  print("Test set score: ", clf_cv.score(X_test, y_test))
  y_score = clf_cv.predict_proba(X_test)

  fpr = [0] * 3
  tpr = [0] * 3
  roc_auc = [0] * 3
  for i in range(3):
      fpr[i], tpr[i], _ = roc_curve(y_test_bin[:, i], y_score[:, i])
      roc_auc[i] = auc(fpr[i], tpr[i])

  # with open("pre_roc_%s.out" % name, 'w') as f:
  #   for i in range(3):
  #     print(",".join([str(f) for f in fpr[i]]), file=f)
  #     print(",".join([str(f) for f in tpr[i]]), file=f)

  print("ROC AUC:", roc_auc)
  print()

run_log_reg = True
run_svc_lin = False
run_svc_rbf = False
run_rand_for = False

# Logistic Regression
if run_log_reg:
  log_reg_params = {
    'penalty': ['l1', 'l2'],
    'C': 10. ** np.linspace(-2,2,20)
  }

  log_reg = GridSearchCV(LogisticRegression(), log_reg_params, cv=5, verbose=2, n_jobs=-1)
  log_reg.fit(X_train, y_train)

# SVC
if run_svc_lin:
  svc_lin_params = {
    'kernel': ['linear'],
    #'C': 10. ** np.linspace(-2,2,5)
    'C': [10. ** -2]
    #'C': [10. ** -2, 10. ** -1, 1]
  }

  svc_lin = GridSearchCV(SVC(probability=True), svc_lin_params, cv=1, verbose=2, n_jobs=-1)
  svc_lin = SVC(probability=True, kernel='linear', C=10.**-2, verbose=2)
  svc_lin.fit(X_train, y_train)

# SVC RBF
if run_svc_rbf:
  svc_rbf_params = {
    'kernel': ['rbf'],
    #'C': 10. ** np.linspace(-2,2,5)
    'C': [10. ** -2]
  }

  svc_rbf = GridSearchCV(SVC(probability=True), svc_rbf_params, cv=3, verbose=2, n_jobs=-1)
  svc_rbf.fit(X_train, y_train)

if run_rand_for:
  rand_for_params = {
    'criterion': ['gini', 'entropy'],
    'n_estimators': range(1000,1001,100)
  }

  rand_for = GridSearchCV(RandomForestClassifier(), rand_for_params, cv=3, verbose=2, n_jobs=-1)
  rand_for.fit(X_train, y_train)

# Scoring
if run_log_reg: score_clf(log_reg, 'log_reg')
if run_svc_lin: score_clf(svc_lin, 'svc_lin')
if run_svc_rbf: score_clf(svc_rbf, 'svc_rbf')
if run_rand_for: score_clf(rand_for, 'rand_for')


