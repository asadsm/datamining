import numpy as np
import pandas as pd

#data = the final dataframe that we export 
#df_ = the original loaded csv file
#trans = the transition dataframe that we modify
reader3 = pd.read_csv('beat-the-bookie-odds-series-football-dataset/odd_series_3.csv', sep=',', quotechar='"', encoding='latin-1', chunksize=1)
first = True
for df_ in reader3:
    data = pd.DataFrame()
    
    # Extract sub-dataframes to modify into windows
    for i in range(0,63):
        if i == 0:
            firstWindow = True
        for j in range(0,3):
            sub = ""
            if j == 0:
                sub = "min"
            if j == 1:
                sub = "max"
            if j == 2:
                sub = "avg"
            trans = pd.DataFrame()
            if firstWindow:
                trans[['match_id','match_date', 'match_time', 'league', 'home_team', 'score_home', 'score_away', 'type']] = df_[['match_id', 'match_date', 'match_time', 'league', 'home_team', 'score_home', 'score_away','type']]
            else:
                trans[['match_id', 'type']] = df_[['match_id', 'type']]
            
            for k in range(0,10):
                trans['t'+str(i+k)+"_"+sub] = df_['t'+str(i+k)+"_"+sub]

            if firstWindow:
                trans = pd.melt(trans, id_vars=['match_id', 'match_date', 'match_time', 'league', 'home_team', 'score_home', 'score_away', 'type'], var_name = 'w'+str(i)+'_'+sub+"_var", value_name='w'+str(i)+'_'+sub)
            else:
                trans = pd.melt(trans, id_vars=['match_id', 'type'], var_name = 'w'+str(i)+'_'+sub+"_var", value_name='w'+str(i)+'_'+sub)
                
            if firstWindow:
                data = pd.concat([trans, data], ignore_index=True)
                        
            else: 
                if j == 0:
                    data['w'+str(i)+'_min_var'] = trans["w"+str(i)+"_min_var"].tolist()
                if j == 1:
                    data['w'+str(i)+'_max_var'] = data["w"+str(i)+"_min_var"].str.replace("min", "max").tolist()
                if j == 2:
                    data['w'+str(i)+'_avg_var'] = data["w"+str(i)+"_min_var"].str.replace("min", "avg").tolist()
            
                data = pd.merge(trans, data, on=['match_id','type','w'+str(i)+'_'+sub+"_var"], how='left')

            firstWindow = False
    
    if first:
         # To rearrange the dataframe columns
        data = data[['match_id', 'match_date', 'match_time', 'league', 'home_team', 'score_home', 'score_away',
                  'type','w0_min','w0_max','w0_avg','w1_min','w1_max','w1_avg','w2_min','w2_max','w2_avg','w3_min','w3_max','w3_avg','w4_min','w4_max','w4_avg','w5_min','w5_max','w5_avg','w6_min','w6_max','w6_avg','w7_min','w7_max','w7_avg','w8_min','w8_max','w8_avg','w9_min','w9_max','w9_avg','w10_min','w10_max','w11_avg'
                'w11_min','w11_max','w11_avg','w12_min','w12_max','w12_avg','w13_min','w13_max','w13_avg','w14_min','w14_max','w14_avg','w15_min','w15_max','w15_avg','w16_min','w16_max','w16_avg','w17_min','w17_max','w17_avg','w18_min','w18_max','w18_avg','w19_min','w19_max','w19_avg','w20_min','w20_max','w20_avg',
                    'w21_min','w21_max','w21_avg','w22_min','w22_max','w22_avg','w23_min','w23_max','w23_avg','w24_min','w24_max','w24_avg','w25_min','w25_max','w25_avg','w26_min','w26_max','w26_avg','w27_min','w27_max','w27_avg','w28_min','w28_max','w28_avg','w29_min','w29_max','w29_avg','w30_min','w30_max','w30_avg',
                    'w31_min','w31_max','w31_avg','w32_min','w32_max','w32_avg','w33_min','w33_max','w33_avg','w34_min','w34_max','w34_avg','w35_min','w35_max','w35_avg','w36_min','w36_max','w36_avg','w37_min','w37_max','w37_avg','w38_min','w38_max','w38_avg','w39_min','w39_max','w39_avg','w40_min','w40_max','w40_avg',
                    'w41_min','w41_max','w41_avg','w42_min','w42_max','w42_avg','w43_min','w43_max','w43_avg','w44_min','w44_max','w44_avg','w45_min','w45_max','w45_avg','w46_min','w46_max','w46_avg','w47_min','w47_max','w47_avg','w48_min','w48_max','w48_avg','w49_min','w49_max','w49_avg','w50_min','w50_max','w50_avg',
                    'w51_min','w51_max','w51_avg','w52_min','w52_max','w52_avg','w53_min','w53_max','w53_avg','w54_min','w54_max','w54_avg','w55_min','w55_max','w55_avg','w56_min','w56_max','w56_avg','w57_min','w57_max','w57_avg','w58_min','w58_max','w58_avg','w59_min','w59_max','w59_avg','w60_min','w60_max','w60_avg',
                    'w61_min','w61_max','w61_avg','w62_min','w62_max','w62_avg']]
        data.to_csv("beat-the-bookie-odds-series-football-dataset/odds_series_time.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding='latin-1')
        first = False
    else:
        data = data[['match_id', 'match_date', 'match_time', 'league', 'home_team', 'score_home', 'score_away',
                  'type','w0_min','w0_max','w0_avg','w1_min','w1_max','w1_avg','w2_min','w2_max','w2_avg','w3_min','w3_max','w3_avg','w4_min','w4_max','w4_avg','w5_min','w5_max','w5_avg','w6_min','w6_max','w6_avg','w7_min','w7_max','w7_avg','w8_min','w8_max','w8_avg','w9_min','w9_max','w9_avg','w10_min','w10_max','w11_avg'
                    'w11_min','w11_max','w11_avg','w12_min','w12_max','w12_avg','w13_min','w13_max','w13_avg','w14_min','w14_max','w14_avg','w15_min','w15_max','w15_avg','w16_min','w16_max','w16_avg','w17_min','w17_max','w17_avg','w18_min','w18_max','w18_avg','w19_min','w19_max','w19_avg','w20_min','w20_max','w20_avg',
                    'w21_min','w21_max','w21_avg','w22_min','w22_max','w22_avg','w23_min','w23_max','w23_avg','w24_min','w24_max','w24_avg','w25_min','w25_max','w25_avg','w26_min','w26_max','w26_avg','w27_min','w27_max','w27_avg','w28_min','w28_max','w28_avg','w29_min','w29_max','w29_avg','w30_min','w30_max','w30_avg',
                    'w31_min','w31_max','w31_avg','w32_min','w32_max','w32_avg','w33_min','w33_max','w33_avg','w34_min','w34_max','w34_avg','w35_min','w35_max','w35_avg','w36_min','w36_max','w36_avg','w37_min','w37_max','w37_avg','w38_min','w38_max','w38_avg','w39_min','w39_max','w39_avg','w40_min','w40_max','w40_avg',
                    'w41_min','w41_max','w41_avg','w42_min','w42_max','w42_avg','w43_min','w43_max','w43_avg','w44_min','w44_max','w44_avg','w45_min','w45_max','w45_avg','w46_min','w46_max','w46_avg','w47_min','w47_max','w47_avg','w48_min','w48_max','w48_avg','w49_min','w49_max','w49_avg','w50_min','w50_max','w50_avg',
                    'w51_min','w51_max','w51_avg','w52_min','w52_max','w52_avg','w53_min','w53_max','w53_avg','w54_min','w54_max','w54_avg','w55_min','w55_max','w55_avg','w56_min','w56_max','w56_avg','w57_min','w57_max','w57_avg','w58_min','w58_max','w58_avg','w59_min','w59_max','w59_avg','w60_min','w60_max','w60_avg',
                    'w61_min','w61_max','w61_avg','w62_min','w62_max','w62_avg']]
        with open("beat-the-bookie-odds-series-football-dataset/odds_series_time.csv", 'a') as f:
            data.to_csv(f, header=False, sep=',', quotechar='"', na_rep = 'nan', index=False, encoding='latin-1')