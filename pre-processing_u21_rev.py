# PRE-PROCESSING
#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib as mpl

# ############# PRE-PROCESSING STEPS: ######################
# 1) Remove U21 and under leagues
data_rev2 = pd.read_csv('2odds_series_u21_25200_29400_out.csv', sep=',', quotechar='"', encoding="latin-1")

#print (data_rev2.iloc[[0,-1]])

###  U16, U19
data_rev2 = data_rev2.loc[data_rev2['league'].str.contains("U" + "1") == False]
data_rev2 = data_rev2.loc[data_rev2['league'].str.contains("U" + "2") == False]
data_rev2 = data_rev2.loc[data_rev2['league'].str.contains("Junior") == False]

data_rev2.to_csv("2odds_series_u21_25200_29400_out_removeU.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")


# #######################################
# data_rev3 = pd.read_csv('odds_series_21100_25300_out_final.csv', sep=',', quotechar='"', encoding="latin-1")


# ###  U16, U19
# data_rev3 = data_rev3.loc[data_rev2['league'].str.contains("U" + "1") == False]
# data_rev3 = data_rev3.loc[data_rev2['league'].str.contains("U" + "2") == False]
# data_rev3 = data_rev3.loc[data_rev2['league'].str.contains("Junior") == False]

# data_rev3.to_csv("odds_series_21100_25300_out_final_removeU.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")

