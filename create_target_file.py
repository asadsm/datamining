import numpy as np
import pandas as pd

def home_win_lose(row):
    home_score = row['score_home']
    away_score = row['score_away']
    return(0 * (home_score > away_score) +
        1 * (home_score < away_score) +
        2 * (home_score == away_score)) 

data = pd.read_csv('odds_series_timesteps_final_ru_rnans_br.csv', sep=',', quotechar='"', encoding="latin-1") 
data=data.convert_objects(convert_numeric=True)
data['target'] = np.nan

# Part I - Add in the target data column using the home_win_lose func
for index, row in data.iterrows():
    data.at[index,'target'] = home_win_lose(row)
    
# Drop the X features that are not used
data = data.drop(['match_id', 'score_home', 'score_away', 'home_team', 'league', 'match_date', 'match_time', 
                         't0', 't1', 't2', 't3', 't4', 't5', 't6', 't7', 't8', 't9', 't10', 
                         't11', 't12', 't13', 't14', 't15', 't16', 't17', 't18', 't19', 't20', 
                         't21', 't22', 't23', 't24', 't25', 't26', 't27', 't28', 't29', 't30', 
                         't31', 't32', 't33', 't34', 't35', 't36', 't37', 't38', 't39', 't40', 
                         't41', 't42', 't43', 't44', 't45', 't46', 't47', 't48', 't49', 't50',
                         't51', 't52', 't53', 't54', 't55', 't56', 't57', 't58', 't59', 't60', 't61'], axis=1)

data.to_csv("odds_series_timesteps_final_target.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")
