#!/usr/bin/python3

import random
from math import isnan
import numpy as np
import pandas as pd
import matplotlib as mpl
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier


bookies = ['Interwetten','bwin','bet-at-home','Unibet','Stan James','Expekt','10Bet','William Hill','bet365','Pinnacle Sports','DOXXbet','Betsafe','Betway','888sport','Ladbrokes','Betclic','Sportingbet','myBet','Betsson','188BET','Jetbull','Paddy Power','Tipico','Coral','SBOBET','BetVictor','12BET','Titanbet','youwin','ComeOn','Betadonis','Betfair Sports']

bookies_outliers = np.zeros(32, dtype=float)
bookies_outliers_agree = np.zeros(32, dtype=float)
bookies_outliers_disagree = np.zeros(32, dtype=float)

bookies_correct = np.zeros(32, dtype=float)
num_matches = 0

train_size = 65000

def train(row):
  home_score = row['score_home']
  away_score = row['score_away']

  result = (0 * (home_score > away_score) +
            1 * (home_score < away_score) +
            2 * (home_score == away_score))

  home_odds = []
  away_odds = []
  draw_odds = []

  for bookie in range(1,33):
    home_odd = row['home_b%d_71' % bookie]
    away_odd = row['away_b%d_71' % bookie]
    draw_odd = row['draw_b%d_71' % bookie]

    if not isnan(home_odd) and not isnan(away_odd) and not isnan(draw_odd):
      home_odds.append(home_odd)
      away_odds.append(away_odd)
      draw_odds.append(draw_odd)

  std_dev = np.std(home_odds), np.std(away_odds), np.std(draw_odds)
  mean = np.mean(home_odds), np.mean(away_odds), np.mean(draw_odds)

  for bookie in range(1,33):
    home_odd = row['home_b%d_71' % bookie]
    away_odd = row['away_b%d_71' % bookie]
    draw_odd = row['draw_b%d_71' % bookie]

    if abs(home_odd - mean[0]) > 2 * std_dev[0] or \
       abs(away_odd - mean[1]) > 2 * std_dev[1] or \
       abs(draw_odd - mean[2]) > 2 * std_dev[2]:
      bookies_outliers[bookie - 1] += 1

      if np.argmin(np.array([home_odd, away_odd, draw_odd])) == result:
        bookies_outliers_agree[bookie - 1] += 1
      else:
        bookies_outliers_disagree[bookie - 1] += 1


    if np.argmin(np.array([home_odd, away_odd, draw_odd])) == result:
      bookies_correct[bookie - 1] += 1

random_correct_bets = 0
random_bets = 0

correct_bets = 0
bets = 0
rejects = 0

def test(row):
  global random_correct_bets
  global random_bets

  global correct_bets
  global bets
  global rejects

  home_score = row['score_home']
  away_score = row['score_away']

  result = (0 * (home_score > away_score) +
            1 * (home_score < away_score) +
            2 * (home_score == away_score))

  home_odds = []
  away_odds = []
  draw_odds = []

  for bookie in range(1,33):
    home_odd = row['home_b%d_71' % bookie]
    away_odd = row['away_b%d_71' % bookie]
    draw_odd = row['draw_b%d_71' % bookie]

    if not isnan(home_odd) and not isnan(away_odd) and not isnan(draw_odd):
      home_odds.append(home_odd)
      away_odds.append(away_odd)
      draw_odds.append(draw_odd)

  std_dev = np.std(home_odds), np.std(away_odds), np.std(draw_odds)
  mean = np.mean(home_odds), np.mean(away_odds), np.mean(draw_odds)

  for bookie in range(1,33):
    home_odd = row['home_b%d_71' % bookie]
    away_odd = row['away_b%d_71' % bookie]
    draw_odd = row['draw_b%d_71' % bookie]

    if abs(home_odd - mean[0]) > 2 * std_dev[0] or \
       abs(away_odd - mean[1]) > 2 * std_dev[1] or \
       abs(draw_odd - mean[2]) > 2 * std_dev[2]:
      # Bet against this bookie's top pick
      if bookies_outliers_priors[bookie - 1] < .45:
        bets += 1
        if np.argmin([home_odd, away_odd, draw_odd]) != result:
          correct_bets += 1
      if bookies_outliers_priors[bookie - 1] > .54:
        bets += 1
        if np.argmin([home_odd, away_odd, draw_odd]) == result:
          correct_bets += 1

  random_bets += 1
  random_correct_bets += 1 if result == random.choice([0,1,2]) else 0

for odds in pd.read_csv(
    'all_series_total.csv', sep=',', quotechar='"', chunksize=10**3, encoding='latin'):
  for idx, row in odds.iterrows():
    num_matches += 1

    if idx == 65000:
      print("Bookie outliers:", bookies_outliers)
      print("Bookie outliers agree:", bookies_outliers_agree)
      print()

      # Chance a bookie is correct given they have an outlier odd
      bookies_outliers_priors = bookies_outliers_agree / bookies_outliers

      print("Bookie accuracy:", bookies_correct / num_matches)
      print("Bookie outlier prior:", bookies_outliers_priors)

    if idx < 65000:
      train(row)
    else:
      test(row)

print("Bookie outlier accuracy:", float(correct_bets) / float(bets))
print("Bookie random accuracy:", float(random_correct_bets) / float(random_bets))
