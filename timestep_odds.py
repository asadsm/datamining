import numpy as np
import pandas as pd

#data = the original loaded csv file
#df_ = the final dataframe that we export
#trans = the transition dataframe that we modify
data = pd.read_csv('beat-the-bookie-odds-series-football-dataset/odds_series_three.csv', sep=',', quotechar='"', encoding='latin-1')
df_ = pd.read_csv('beat-the-bookie-odds-series-football-dataset/odds_series_three.csv', sep=',', quotechar='"', encoding='latin-1')

# Extract sub-dataframes to modify into windows
for i in range(0,63):
    for j in range(0,3):
        sub = ""
        if j == 0:
            sub = "min"
        if j == 1:
            sub = "max"
        if j == 2:
            sub = "avg"
        trans = data[['match_id','league','home_team','match_date','match_time','score_home','score_away','bookie','type']].copy()
        for k in range(0,10):
            trans['t'+str(i+k)+"_"+sub] = data['t'+str(i+k)+"_"+sub]
                
        pd.melt(trans, id_vars=['match_id','league','home_team','match_date','match_time','score_home','score_away','bookie','type'], value_name='w'+str(i))
        #print(trans)
        pd.merge(df_, trans, on=['match_id','type','bookie'])
        
print(df_)

df_.to_csv("beat-the-bookie-odds-series-football-dataset/odds_series_timestep.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding='latin-1')
