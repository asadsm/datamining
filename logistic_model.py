#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib as mpl
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier

data = pd.read_csv('data/closing_odds.csv.gz', compression='gzip', sep=',', quotechar='"')

# 1. match_table_id: unique identifier of the game
# 2. league of the game
# 3. match date
# 4. home team
# 5. 90-minute score of home team
# 6. away team
# 7. 90-minute score of away team
# 8. average closing odds home win
# 9. average closing odds draw
# 10. average closing odds away win
# 11. maximum offered closing odds home win
# 12. maximum offered closing odds draw
# 13. maximum offered closing odds away win
# 14. name of bookmaker offering maximum closing odds for home win
# 15. name of bookmaker offering maximum closing odds for draw
# 16. name of bookmaker offering maximum closing odds for away win
# 17. number of available closing odds for home win
# 18. number of available closing odds for draw
# 19. number of available closing odds for away win

n_games = data.shape[0]

leagues = data['league']
n_leagues = pd.unique(data['league']).shape[0]

prior_home = float(sum(data['home_score'] > data['away_score'])) / n_games
prior_draw = float(sum(data['home_score'] == data['away_score'])) / n_games
prior_away = float(sum(data['home_score'] < data['away_score'])) / n_games

# print('Total number of games: ' + str(n_games) + "\n");
# print('Total number of Leagues:' + str(n_leagues) + "\n");
# print('Proportion of Home victories: ' + str(prior_home) + "\n");
# print('Proportion of Draws: ' + str(prior_draw) + "\n");
# print('Proportion of Away victories: ' + str(prior_away) + "\n");


def home_win_lose(row):
    if row['home_score'] > row['away_score']:
        return 1
    elif row['home_score'] == row['away_score']:
        return 0
    else:
        return -1


# Add column which represents home win, draw or loss [1, 0, -1]
data['home_win'] = data.apply(lambda row: home_win_lose(row), axis=1)

# Drop columns which aren't useful
processed_data = data.drop('match_id', axis=1)
old_length = len(processed_data)
processed_data = data.loc[data['away_score'] < 30].loc[data['home_score'] < 30]
print('Dropped ' + str(old_length - len(processed_data)) + ' as score outliers')

# Separate data into input and output
X = processed_data.drop(['home_win', 'home_score', 'away_score', 'top_bookie_home_win', 'top_bookie_draw', 'top_bookie_away_win', 'home_team', 'away_team', 'league', 'match_date'], axis=1)
y = processed_data['home_win']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Create Logistic Regression model and use GridSearchCV to tune parameters
'''
log_reg = LogisticRegression()
log_reg.fit(X_train, y_train)
y_predict = log_reg.predict(X_test)
test_MSE = mean_squared_error(y_test, y_predict)
print("MSE of LogReg Model: " + str(test_MSE))
'''

tree_clf = DecisionTreeClassifier(random_state=42)
tree_clf.fit(X_train, y_train)
y_predict = tree_clf.predict(X_test)
tree_MSE = mean_squared_error(y_test, y_predict)
print("MSE of Decision Tree Model: " + str(tree_MSE))

