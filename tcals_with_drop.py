import numpy as np
import pandas as pd

reader2 = pd.read_csv('beat-the-bookie-odds-series-football-dataset/all_series_modified.csv', sep=',', quotechar='"', encoding='latin-1', chunksize=2)
first = True
for df in reader2:
    for i in range (0,72):
        df["t" + str(i) + "_avg"]=df.groupby(["match_id", "type"])["t" + str(i)].transform('mean')
        df["t" + str(i) + "_min"]=df.groupby(["match_id", "type"])["t" + str(i)].transform('min')
        df["t" + str(i) + "_max"]=df.groupby(["match_id", "type"])["t" + str(i)].transform('max')

    df.drop(['bookie','t0','t1','t2','t3','t4','t5','t6','t7','t8','t9','t10',
                't11','t12','t13','t14','t15','t16','t17','t18','t19','t20',
                't21','t22','t23','t24','t25','t26','t27','t28','t29','t30',
                't31','t32','t33','t34','t35','t36','t37','t38','t39','t40',
                't41','t42','t43','t44','t45','t46','t47','t48','t49','t50',
                't51','t52','t53','t54','t55','t56','t57','t58','t59','t60',
                't61','t62','t63','t64','t65','t66','t67','t68','t69','t70', 't71'], axis=1, inplace = True)   
    df.drop_duplicates(inplace=True)
    
    if first:
        df.to_csv("beat-the-bookie-odds-series-football-dataset/all_series_tcals.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding='latin-1')
        first = False
    else:
         with open("beat-the-bookie-odds-series-football-dataset/all_series_tcals.csv", 'a') as f:
            df.to_csv(f, sep=',', quotechar='"', header=False, na_rep = 'nan', index=False, encoding='latin-1')