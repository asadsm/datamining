import numpy as np
import pandas as pd
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import mean_squared_error, accuracy_score, roc_auc_score, roc_curve, auc
from sklearn.preprocessing import label_binarize

data = pd.read_csv('timestep_outputs/odds_series_for_models.csv', sep=',', quotechar='"', encoding="latin-1") 
data=data.convert_objects(convert_numeric=True)

# Part II - Predict which team wins
# Drop score outliers
#data = data.loc[data['score_away'] < 30].loc[data['score_home'] < 30]

# Dummy code - type and bookie
data = pd.get_dummies(data, columns=['type','bookie'])

y = data['result']
data = data.fillna(0)
X = data.drop(['result'], axis=1)

X_train, X_test, y_train, y_test = train_test_split(X,y,test_size = 0.33, random_state=42)
#print(X_train.shape, y_train.shape, X_test.shape, y_test.shape)

y_test_bin = label_binarize(y_test, classes=[0,1,2])

def home_win_lose(row):
    home_score = row['score_home']
    away_score = row['score_away']
    return(0 * (home_score > away_score) +
        1 * (home_score < away_score) +
        2 * (home_score == away_score)) 

def score_clf(clf_cv, name):
  print(name, "scores:")
  print("Hyper params: " + str(clf_cv.best_params_))
  print("Test set score: ", clf_cv.score(X_test, y_test))
  y_score = clf_cv.predict_proba(X_test)
  print("y_score:", y_score)  

  fpr = [0] * 3
  tpr = [0] * 3
  roc_auc = [0] * 3
  for i in range(3):
      fpr[i], tpr[i], _ = roc_curve(y_test_bin[:, i], y_score[:, i])
      roc_auc[i] = auc(fpr[i], tpr[i])

  #with open("pre_roc_%s.out" % name, 'w') as f:
    #for i in range(3):
      #print(",".join([str(f) for f in fpr[i]]), file=f)
      #print(",".join([str(f) for f in tpr[i]]), file=f)

  print("ROC AUC:", roc_auc)
  print()

run_log_reg = False
run_mlp_reg = False
run_rand_for = True
adaboost= False
grad_descent = False

# Logistic Regression
if run_log_reg:
  log_reg_params = {
    'penalty': ['l1', 'l2'],
    'C': 10. ** np.linspace(-2,2,20)
  }

  log_reg = GridSearchCV(LogisticRegression(), log_reg_params, cv=2, verbose=2, n_jobs=-1)
  log_reg.fit(X_train, y_train)

# Random Forests
if run_rand_for:
  rand_for_params = {
    'criterion': ['gini', 'entropy'],
    'n_estimators': range(1000,1001,100)
  }

  rand_for = GridSearchCV(RandomForestClassifier(), rand_for_params, cv=2, verbose=2, n_jobs=-1)
  rand_for.fit(X_train, y_train)

#AdaBoost
if adaboost:
    parameters_ada = {'n_estimators':[50,300,400,700,1000], 'learning_rate':[0.01, 0.1, 1, 10]}
    clf_ada = GridSearchCV(estimator=AdaBoostClassifier(), param_grid=parameters_ada)
    clf_ada.fit(X_train, y_train)

# GBDT
if grad_descent:
    parameters_gbdt = {'n_estimators':[50,300,400,700,1000], 'learning_rate':[0.01, 0.1, 1, 10], 'max_depth':[10,50,100,150]}
    clf_gbdt = GridSearchCV(estimator=GradientBoostingClassifier(), param_grid=parameters_gbdt)
    clf_gbdt.fit(x_train, y_train)

# Scoring
if run_log_reg: score_clf(log_reg, 'log_reg')
if run_mlp_reg: score_clf(crf_mlp, 'crf_mlp')
if run_rand_for: score_clf(rand_for, 'rand_for')
if adaboost: score_clf(clf_ada, 'clf_ada')
if grad_descent: score_clf(clf_gbdt, 'clf_gbdt')