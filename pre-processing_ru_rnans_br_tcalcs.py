# PRE-PROCESSING
#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib as mpl


############################ for each match_id, type, and time step, calculate the mean, max, and min odds

# df_ = pd.read_csv('all_series_odds_modified.csv', sep=',', quotechar='"', encoding="latin-1")

df_ = pd.read_csv('odds_series_timesteps_final_ru_rnans_br.csv', sep=',', quotechar='"', encoding="latin-1")

#print (df_.iloc[[0,-1]])
#print (df_.shape)

######## subset of top 50 % of bookies: 
### smaller rank means play more so want playing_rank <= 16
data_new = df_[df_['playing_rank'] < 17]
# print (data_new.shape)

# data_rev = data_new[:100]
# data_rev.to_csv("odds_series_timesteps_final_ru_rnans_br_100_rank.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")

############################ randomly sample 100 matches

sample_data = data_new.groupby(['match_id', 'type']).apply(lambda s: s.sample(1))

#print (sample_data.shape)

#print (sample_data[:4])

sample_data.to_csv("odds_series_timesteps_final_ru_rnans_br_sample.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")


############################# change data type

for i in range (0, 72):
	df_["t" + str(i) ]=pd.to_numeric(df_["t" + str(i) ], errors='coerce')

# df_["t0"] = pd.to_numeric(df_.t0, errors='coerce')


# print (df_.dtypes["t0"])
# print (df_.dtypes["t40"])
# print (df_.dtypes["t70"])

############################# calc t_avg, min, max
for i in range (0, 72):

	df_["t" + str(i) + "_avg"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('mean')
	df_["t" + str(i) + "_min"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('min')
	df_["t" + str(i) + "_max"]=df_.groupby(["match_id", "type"])["t" + str(i)].transform('max')

df_.to_csv("odds_series_tcalcs_final.csv", sep=',', quotechar='"', na_rep = 'nan', index=False, encoding="latin-1")
