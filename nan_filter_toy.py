### This code removes all matches that have all NaNs 
### and then determine which leagues to discard.
#!/usr/bin/python3

import numpy as np
import pandas as pd
import matplotlib as mpl


data = pd.read_csv("odds_series_toy.csv")

#print (data.shape)

# per row, count the number of NaNs 
data['sum_nan']=data.isnull().sum(axis=1)

# per bookie, count the number of NaNs 
data['sum_nan_bookie'] = data.groupby('bookie', sort=False)["sum_nan"].transform('sum')



#rank bookies based on sum_nan_bookie (how much each bookie is playing)
data['playing_rank'] = data['sum_nan_bookie'].rank(method='dense', ascending=True)


data['sum_nan_match'] = data.groupby(['match_id', 'bookie'], sort=False)["sum_nan"].transform('sum')

print (data.shape)
########### remove rows that have all NaNs #####################
####### CHANGE 33
data_rev = data.loc[data['sum_nan_match'] != 33]

print (data_rev.shape)


data_rev.to_csv("odds_series_toy_rank.csv", sep=',', quotechar = '"', na_rep='nan')


		