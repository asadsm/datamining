#### This code checks for multicolinnearity by plotting a diagonal 
#### correlation matrix.
#### Source: https://seaborn.pydata.org/examples/many_pairwise_correlations.html


import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

data = pd.read_csv("closing_odds.csv") 
#print (data.shape)


### only keep home_score and away_score less than 100
data_rev = data.loc[data['home_score'] < 100]

data_rev2 = data_rev.loc[data_rev['away_score'] < 100]

#print (data_rev2.shape)

### remove columns not needed for plot
col_list= list(data)
col_list.remove('match_id')
col_list.remove('match_date')

col_list
#print (col_list)

data_rev3 = data_rev2.loc[:, col_list]

#print (data_rev3[:5])

# Compute the correlation matrix
corr = data_rev3.corr()


# ############ method 1: #############
#Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

sns.set(font_scale=1.4)

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(18, 16))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 1, as_cma0p=True)

# Draw the heatmap with the mask and correct aspect ratio
a = sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})

plt.setp(ax.get_xticklabels(), rotation=90)
plt.setp(ax.get_yticklabels(), rotation=0)

plt.show()



# # ############ method 1: #############
# #Generate a mask for the upper triangle
# mask = np.zeros_like(corr, dtype=np.bool)
# mask[np.triu_indices_from(mask)] = True

# sns.set(font_scale=1.6)

# # Set up the matplotlib figure
# f, ax = plt.subplots(figsize=(30, 50))

# # Generate a custom diverging colormap
# cmap = sns.diverging_palette(220, 10, as_cmap=True)

# # Draw the heatmap with the mask and correct aspect ratio
# sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
#             square=True, linewidths=.5, cbar_kws={"shrink": .5})

# # sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
# #             square=True, linewidths=.5, xticklabels=1, yticklabels=1, cbar_kws={"shrink": .5})

# plt.show()

#######################################


# ############ method 2: #############
# # plot the heatmap
# sns.heatmap(corr, 
#         xticklabels=corr.columns,
#         yticklabels=corr.columns)

# plt.show()

####################################### 




